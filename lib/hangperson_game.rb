class HangpersonGame

  # add the necessary class methods, attributes, etc. here
  # to make the tests in spec/hangperson_game_spec.rb pass.

  # Get a word from remote "random word" service

  # def initialize()
  # end
  
  def initialize(word)
    @word = word.to_s.downcase
    @guesses = ''
    @wrong_guesses = ''

  end

  attr_accessor :word, :guesses, :wrong_guesses
  
  def guess(letter)
    
    raise (ArgumentError) if letter.nil? || letter.empty?
    raise (ArgumentError) unless letter =~ /[[:alpha:]]/

    letter.to_s.downcase!
    
    if @word.include? letter
      if @guesses.include? letter
        return false
      end
      
      @guesses += letter
      
      return letter
      
    else  
      
      if @wrong_guesses.include? letter
        return false
      end
      
      @wrong_guesses += letter
      
      return letter
      
    end
  end


def word_with_guesses
  new_word = ''
  
  for i in 0..@word.length
  
    if @guesses.include? @word[i].to_s
      new_word << @word[i].to_s
    else
      new_word << '-'
    end
    
  end

  return new_word

end


def check_win_or_lose
  return :win if @word == word_with_guesses
  return :lose if @wrong_guesses.length >= 7
  return :play
end


  def self.get_random_word
    require 'uri'
    require 'net/http'
    uri = URI('http://watchout4snakes.com/wo4snakes/Random/RandomWord')
    Net::HTTP.post_form(uri ,{}).body
  end

end
